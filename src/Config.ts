export interface Config {
    url: string;
    username?: string;
    password?: string;
    token?: string;
    uID?: string;
}

/**
 * @class ClientConfig
 *
 * @author VoidNulll
 */
class ClientConfig implements Config {
    /**
     * @param {Object<Config>} config The Evolve-Master config
     * @param {String} config.url The url for the evolve-x instance
     * @param {String} [config.username] The authenticating users username
     * @param {String} [config.password] The password to authenticate with for account endpoints
     * @param {String} [config.token] The users API token
     * @param {String} [config.uID] The users evolve-x identifier
     *
     * @prop {String} url The url for the evolve-x instance
     * @prop {String} username The username for the user
     * @prop {String} password Users password
     * @prop {String} [token] Users API token
     * @prop {String} uID Users identifier.
     */
    public url: string;

    public username?: string;

    public password?: string;

    public token?: string;

    public uID?: string;

    constructor(config: Config) {
        this.url = config.url;
        this.username = config.username;
        this.password = config.password;
        this.token = config.token;
        this.uID = config.uID;
    }
}

export default ClientConfig;
