import superagent from 'superagent';
import ClientConfig, { Config } from './Config';
import Codes from './Codes';
import Utils from './Utils';

export interface AccountResponse {
    owner: boolean;
    username: string;
    admin: boolean;
    uID: string;
    token_generated: boolean;
}

export interface PassAuth {
    password?: string;
    username?: string;
}

export interface TokenAuth {
    uid?: string;
    token?: string;
}

export interface Account {
    owner?: boolean;
    username?: string;
    admin?: boolean;
}

export interface Notification {
    ID: string;
    title: string;
    notify: string;
}

export interface Short {
    link: string;
    owner: string;
    ID: string;
}

export interface File {
    file: Buffer;
    name: string;
}

export type Uploadable = File | string;

/**
 * @class BaseClient
 *
 * The Evolve Master client
 *
 * @param {Object<Config>} config The Evolve-Master config
 * @param {String} config.url The url for the evolve-x instance
 * @param {String} config.username The authenticating users username
 * @param {String} config.password The password to authenticate with for account endpoints
 * @param {String} [config.token] The users API token
 * @param {String} config.uID The users evolve-x identifier
 *
 * @prop {Object<Config>} config The actual Evolve-Master config
 *
 * @prop {Object<PassAuth>} passAuth The password authentication object
 * @prop {String} passAuth.username The authentications username
 * @prop {String} passAuth.password The authentication password
 *
 * @prop {Object<TokenAuth>} tokenAuth The token authentication object
 * @prop {String} tokenAuth.uid The users identifier
 * @prop {String} tokenAuth.token The users token to the evolve-x API
 *
 * @prop {Object<Account>} account Some basic account info, useful for internal stuff
 * @prop {Boolean} account.owner Whether or not the user owns this evolve-x instance
 * @prop {String} account.username The users username
 * @prop {Boolean} account.admin Whether or not the user is an admin on this evolve-x instance
 */
class Client {
    public config: any;

    private passAuth: PassAuth;

    private tokenAuth: TokenAuth;

    private account: Account;

    constructor(config: Config) {
        this.config = config;
        this.passAuth = {};
        this.tokenAuth = {};
        this.account = {};
    }

    /**
     * Initialize the config. Kill process if config cannot init.
     *
     * @private
     */
    private async _initConfig(config?: Config): Promise<void> {
        this.config = new ClientConfig(config || this.config);
        this.passAuth = { username: this.config.username, password: this.config.password };
        await this.fetchAccount();
        this.tokenAuth = { uid: this.config.uID, token: this.config.token };
    }

    /**
     * Refresh the account infos
     *
     * @param {Object<AccountResponse>} account The new account.
     * @private
     */
    private _refreshAcc(account: AccountResponse): void {
        this.account = { owner: account.owner, admin: account.admin, username: account.username };
    }

    /**
     * Initialize the config, and basically the app.
     *
     * @returns {Promise<Object<Config>>}
     */
    public async init(): Promise<Config> {
        await this._initConfig();
        return this.config;
    }

    /**
     * Update the configuration for the app.
     * @param {Object<Config> | String} config
     * @param {String} [key] The key to update, if you are updating a single key.
     *
     * @returns {Promise<Object<Config>>}
     */
    public async updateConfig(config: Config | string, key?: string): Promise<Config> {
        Utils.paramsCheck(1, [config] );
        if (typeof config === 'string') {
            Utils.paramsCheck(2, [config, key] );
            if (key === 'username') {
                this.passAuth.username = config;
                this.config.username = config;
            }
            if (key === 'url') {
                this.config.url = config;
            }
            if (key === 'password') {
                this.passAuth.password = config;
                this.config.password = config;
            }
            if (key === 'token') {
                this.tokenAuth.token = config;
                this.config.token = config;
            }
            if (key === 'uID') {
                this.tokenAuth.uid = config;
                this.config.uID = config;
            }
        } else {
            this._initConfig(config);
        }
        return this.config;
    }

    /**
     * Add a user as admin
     * @param {String} uID The ID of the user who to give admin powers
     * @returns {Promise<String>}
     */
    public async addAdmin(uID: string): Promise<string> {
        Utils.paramsCheck(1, [uID] );
        if (!this.account.owner) {
            throw Error('[AUTH ERROR] Owner only method!');
        }
        const match = uID.match(/[0-9]{18, 22}/);
        if (!match || match.length !== uID.length) {
            throw Error('[ERROR] ID is not a valid Evolve-X ID!');
        }

        let res;
        try {
            res = await superagent.post(`${this.config.url}/api/admin?uid${uID}`).set(this.passAuth);
        } catch (err) {
            if (err.response.notFound) {
                throw Error('[ERROR] User not found!');
            }
            Utils.standardError(err);
        }
        if (!res) {
            throw Error('Missing response!');
        }

        if (res.text.startsWith('[ERROR]') ) {
            throw Error(res.text);
        }
        return res.text;
    }

    /**
     * Clear your user notifications
     * @returns {Promise<String>}
     */
    public async clearNotifications(): Promise<string> {
        let res;
        try {
            res = await superagent.delete(`${this.config.url}/api/notifications`).set(this.tokenAuth);
        } catch (err) {
            Utils.standardError(err);
        }
        if (!res) {
            throw Error('Missing response!');
        }

        return res.text;
    }

    /**
     * Shorten a URL
     * @param {String} url The URL to shorten
     * @returns {Promise<String>}
     */
    public async createShort(url: string): Promise<string> {
        Utils.paramsCheck(1, [url] );
        let res;
        try {
            res = await superagent.post(`${this.config.url}/api/short`).set(this.tokenAuth).send( { url } );
        } catch (err) {
            if (err.response.notFound) {
                throw Error('[ERROR] Invalid URL');
            }
            Utils.standardError(err);
        }
        if (!res) {
            throw Error('Missing response!');
        }

        return res.text;
    }

    /**
     * Delete yours or another users account
     * @param {String} [uID] Optional user ID to base off of
     * @returns {Promise<String>}
     */
    public async deleteAccount(uID?: string): Promise<string> {
        if (uID) {
            if (!this.account.admin) {
                throw Error('[USER ERROR] You are not an admin! Cannot delete others accounts!');
            } if (this.account.owner && uID === this.config.uID) {
                throw Error('[USER ERROR] The owner account may not be deleted!');
            } if (this.config.uID === uID) {
                throw Error('[USER ERROR] You may not delete your account this way!');
            }
        } else if (this.account.owner) {
            throw Error('[USER ERROR] The owner account may not be deleted!');
        }
        let res;
        try {
            let reqUrl = `${this.config.url}/api/account`;
            if (uID) {
                reqUrl += `?uid=${uID}`;
            }
            res = await superagent.delete(reqUrl).set(this.passAuth);
        } catch (err) {
            if (err.response.forbidden) {
                throw Error('[ERROR] You cannot delete that account!');
            }
            if (err.response.notFound) {
                throw Error('[ERROR] That account does not exist!');
            }
            Utils.standardError(err);
        }
        if (!res) {
            throw Error('Missing response!');
        }

        return res.text;
    }

    /**
     * Delete an admin notification
     * @param {String} ID The ID of the notification you want to delete
     * @returns {Promise<String>}
     */
    public async deleteAdminNotification(ID: string): Promise<string> {
        if (!this.account.admin) {
            throw Error('[USER ERROR] Account not set as admin! Try fetching the account!');
        }
        Utils.paramsCheck(1, [ID] );
        let res;
        try {
            res = await superagent.delete(`${this.config.url}/api/admin_notification?id=${ID}`).set(this.tokenAuth);
        } catch (err) {
            if (err.response.notFound) {
                throw Error(err.response.text);
            }
            Utils.standardError(err);
        }

        if (!res) {
            throw Error('Missing response!');
        }

        return res.text;
    }

    /**
     * Delete an Image
     *
     * @param {String} ID The Image ID
     * @returns {Promise<string>}
     */
    public async deleteImage(ID: string): Promise<string> {
        Utils.paramsCheck(1, [ID] );
        let res;
        try {
            res = await superagent.delete(`${this.config.url}/api/image?id=${ID}`).set(this.tokenAuth);
        } catch (err) {
            if (err.response.notFound) {
                throw Error(`[ERROR] Image with ID ${ID} not found!`);
            }
            Utils.standardError(err);
        }
        if (!res) {
            throw Error('Missing response!');
        }

        return res.text;
    }

    /**
     * Delete a notification
     * @param {String} ID The ID of the notification you want to delete
     * @returns {Promise<String>}
     */
    public async deleteNotification(ID: string): Promise<string> {
        Utils.paramsCheck(1, [ID] );
        let res;
        try {
            res = await superagent.delete(`${this.config.url}/api/notification?id=${ID}`).set(this.tokenAuth);
        } catch (err) {
            if (err.response.notFound) {
                throw Error(err.response.text);
            }
            Utils.standardError(err);
        }

        if (!res) {
            throw Error('Missing response!');
        }

        return res.text;
    }

    /**
     * Delete a short
     * @param {String} ID The shorts ID
     * @returns {Promise<String>}
     */
    public async deleteShort(ID: string): Promise<string> {
        Utils.paramsCheck(1, [ID] );
        let res;
        try {
            res = await superagent.delete(`${this.config.url}/api/short?id=${ID}`).set(this.tokenAuth);
        } catch (err) {
            if (err.response.notFound) {
                throw Error(err.response.text);
            }
            Utils.standardError(err);
        }
        if (!res) {
            throw Error('Missing response!');
        }

        return res.text;
    }

    /**
     * Deny an account
     * @param {String} uID The ID of the users account to deny
     * @param {String} verifyToken The verification token for the user
     * @returns {Promise<String>}
     */
    public async denyAccount(uID: string, verifyToken: string): Promise<string> {
        Utils.paramsCheck(2, [uID, verifyToken] );
        if (!this.account.admin) {
            throw Error('[USER ERROR] Account not set as admin! Try fetching the account!');
        }
        let res;
        try {
            res = await superagent.delete(`${this.config.url}/api/verify`).set(this.tokenAuth).send( { uid: uID, token: verifyToken } );
        } catch (err) {
            if (err.response.notFound) {
                throw Error('[ERROR] User not found!');
            }
            Utils.standardError(err);
        }
        if (!res) {
            throw Error('Missing response!');
        }

        return res.text;
    }

    /**
     * Fetch your account infos.
     *
     * @returns {Promise<AccountResponse>}
     */
    public async fetchAccount(): Promise<AccountResponse> {
        let res;
        try {
            res = await superagent.get(`${this.config.url}/api/account`).set(this.passAuth);
        } catch (err) { // The only error this can return is a 401 (unauthorized)
            Utils.standardError(err);
        }
        if (!res) {
            throw Error('Missing response!');
        }
        const parsed = JSON.parse(res.text);
        this._refreshAcc(parsed);
        return parsed;
    }

    /**
     * Fetch an admin notification by ID
     * @param {String} ID The notification ID
     * @returns {Promise<Object<Notification>>}
     */
    public async fetchAdminNotification(ID: string): Promise<Notification> {
        if (!this.account.admin) {
            throw Error('[ERROR] Account not set as admin! Try fetching the account!');
        }
        Utils.paramsCheck(1, [ID] );
        let res;
        try {
            res = await superagent.get(`${this.config.url}/api/admin_notification?id=${ID}`).set(this.tokenAuth);
        } catch (err) {
            Utils.standardError(err);
        }
        if (!res) {
            throw Error('Missing response!');
        }

        return JSON.parse(res.text);
    }

    /**
     * Fetch all your images as urls
     *
     * @returns {Promise<String[]|[]>}
     */
    public async fetchImages(): Promise<string[]|[]> {
        let res;
        try {
            res = await superagent.get(`${this.config.url}/api/images`).set(this.tokenAuth);
        } catch (err) {
            Utils.standardError(err);
        }
        if (!res) {
            throw Error('Missing response!');
        }
        if (!res.text) {
            return [];
        }
        return JSON.parse(res.text);
    }

    /**
     * Fetch a notification by ID
     * @param {String} ID The notification ID
     * @returns {Promise<Object<Notification>>}
     */
    public async fetchNotification(ID: string): Promise<Notification> {
        Utils.paramsCheck(1, [ID] );
        let res;
        try {
            res = await superagent.get(`${this.config.url}/api/notification?id=${ID}`).set(this.tokenAuth);
        } catch (err) {
            Utils.standardError(err);
        }
        if (!res) {
            throw Error('Missing response!');
        }

        return JSON.parse(res.text);
    }

    /**
     * Fetch account notifications
     * @param {Boolean} [admin] Whether or not to query for admin notifications
     *
     * @returns {Promise<Object<Notification>[]|[]>}
     */
    public async fetchNotifications(admin?: boolean): Promise<Notification[]|[]> {
        if (admin && !this.account.admin) {
            throw Error('[ERROR] Account not set as admin! Try fetching the account!');
        }
        let res;
        try {
            let reqUrl = `${this.config.url}/api/notifications`;
            if (admin) {
                reqUrl += '?admin=true';
            }
            res = await superagent.get(reqUrl).set(this.tokenAuth);
        } catch (err) {
            Utils.standardError(err);
        }
        if (!res) {
            throw Error('Missing response!');
        }

        if (res.noContent) {
            return [];
        }
        return JSON.parse(res.text);
    }

    /**
     * Fetch all your shorts
     *
     * @returns {Promise<Object<Short>[]|[]>}
     */
    public async fetchShorts(): Promise<Short|[]> {
        let res;
        try {
            res = await superagent.get(`${this.config.url}/api/shorts`).set(this.tokenAuth);
        } catch (err) {
            Utils.standardError(err);
        }
        if (!res) {
            throw Error('Missing response!');
        }
        if (!res.text) {
            return [];
        }
        return JSON.parse(res.text);
    }

    /**
     * Remove a users admin authority
     * @param {String} uID The ID of the user who to remove from admin powers
     * @returns {Promise<String>}
     */
    public async removeAdmin(uID: string): Promise<string> {
        Utils.paramsCheck(1, [uID] );
        if (!this.account.owner) {
            throw Error('[AUTH ERROR] Owner only method!');
        }
        const match = uID.match(/[0-9]{18, 22}/);
        if (!match || match.length !== uID.length) {
            throw Error('[ERROR] ID is not a valid Evolve-X ID!');
        }

        let res;
        try {
            res = await superagent.delete(`${this.config.url}/api/admin?uid${uID}`).set(this.passAuth);
        } catch (err) {
            if (err.response.notFound) {
                throw Error('[ERROR] User not found!');
            }
            Utils.standardError(err);
        }
        if (!res) {
            throw Error('Missing response!');
        }

        if (res.text.startsWith('[ERROR]') ) {
            throw Error(res.text);
        }
        return res.text;
    }

    /**
     * Signup for the evolve-x instance
     * @param {String} username The username you wish to have
     * @param {String} password The password you will use
     * @returns {Promise<String>}
     */
    async signup(username: string, password: string): Promise<string> {
        Utils.paramsCheck(2, [username, password] );
        const body = { username, password };
        let res;
        try {
            res = await superagent.post(`${this.config.url}/api/signup`).send(body);
        } catch (err) {
            if (err.response.code === Codes.used) {
                throw Error('[ERROR] Username taken!');
            }
            if (err.response.badRequest) {
                throw Error(`[ERROR] Bad request error: ${err.response.text.slice(8)}`);
            }
            Utils.standardError(err);
        }
        if (!res) {
            throw Error('Missing response!');
        }

        return res.text;
    }

    /**
     * Update your account.
     *
     * @param {String} key The key to update. Either username, or password
     * @param {String} newKey The new info to set the key to.
     *
     * @returns {Promise<String>}
     */
    public async updateAccount(key: string, newKey: string): Promise<string> {
        Utils.paramsCheck(2, [key, newKey] );
        const keys: any = {
            username: 0,
            password: 1,
        };
        const aKey = keys[key];
        if (!aKey) {
            throw Error(`[ERROR] Key ${key} not found!`);
        }
        if (this.config[key] === newKey) {
            throw Error('[ERROR] New key may not be old key!');
        }
        let req;
        try { // eslint-disable-next-line @typescript-eslint/camelcase
            req = await superagent.patch(`${this.config.url}/api/account?key=${aKey}`).set(this.passAuth).send( { new_key: newKey } );
        } catch (err) {
            Utils.standardError(err);
        }
        if (!req) {
            throw Error('Missing response!');
        }
        return req.text;
    }

    /**
     * Update your accounts token.
     *
     * @param {Boolean} force Whether or not to force token updating.
     *
     * @returns {Promise<String>}
     */
    public async updateToken(force?: boolean): Promise<string> {
        const isForce = !!force;
        if (this.config.token && !isForce) {
            throw Error('[ERROR] Token found. Use force to update!');
        }
        let reqUrl = `${this.config.url}/api/token`;
        if (isForce) {
            reqUrl += '?flags=force';
        }
        let res;
        try {
            res = await superagent.post(reqUrl).set(this.passAuth);
        } catch (err) {
            const needForce = 'You have a token, so you need to use force.\nThis will erase your current token';
            if (err.response.forbidden) {
                throw Error(needForce);
            }
            Utils.standardError(err);
        }
        if (!res) {
            throw Error('Missing response!');
        }
        return res.text;
    }

    /**
     * Upload an image
     * @param {Uploadable} file The buffer for uploading.
     * @returns {Promise<String>}
     */
    public async uploadImage(file: Uploadable): Promise<string> {
        Utils.paramsCheck(2, [file] );
        let res;
        try {
            if (typeof file === 'string') {
                res = await superagent.post(`${this.config.url}/api/upload`).set(this.tokenAuth).attach('image', file);
            } else {
                res = await superagent.post(`${this.config.url}/api/upload`).set(this.tokenAuth).attach('image', file.file, file.name);
            }
        } catch (err) {
            Utils.standardError(err);
        }

        if (!res) {
            throw Error('Missing response!');
        }

        return res.text;
    }

    /**
     * Verify an account via the API
     * @param {String} uID The ID of the users account to verify
     * @param {String} verifyToken The verification token for the user
     * @returns {Promise<String>}
     */
    public async verifyAccount(uID: string, verifyToken: string): Promise<string> {
        Utils.paramsCheck(2, [uID, verifyToken] );
        if (!this.account.admin) {
            throw Error('[USER ERROR] Account not set as admin! Try fetching the account!');
        }
        let res;
        try {
            res = await superagent.post(`${this.config.url}/api/verify`).set(this.tokenAuth).send( { uid: uID, token: verifyToken } );
        } catch (err) {
            if (err.response.notFound) {
                throw Error('[ERROR] User not found!');
            }
            Utils.standardError(err);
        }
        if (!res) {
            throw Error('Missing response!');
        }

        return res.text;
    }
}

export default Client;
