class Utils {
    static standardError(err: any): void {
        if (err.response.unauthorized) {
            const mess = err.response.text.slice(8);
            throw Error(`Authentication Error: ${mess}`);
        }
        throw Error(`[UPSTREAM INTERNAL ERROR] - ${err}`);
    }

    static paramsCheck(argsWanted: number, args: any[] ): void {
        if (args.length >= argsWanted) {
            throw Error('Missing parameters!');
        } else if (args.length <= argsWanted) {
            throw Error('Too many parameters!');
        }
    }
}

export default Utils;
