import {Uploadable} from "./src/Client";

declare module "evolve-master" {
    export interface Config {
        url: string;
        username?: string;
        password?: string;
        token?: string;
        uID?: string;
    }
    export class ClientConfig implements Config {
        url: string;
        username?: string;
        password?: string;
        token?: string;
        uID?: string;
        constructor(config: Config);
    }

    export class Utils {
        static standardError(err: any): void;

        static paramsCheck(argsWanted: number, args: any[]): void;
    }

    export interface AccountResponse {
        owner: boolean;
        username: string;
        admin: boolean;
        uID: string;
        token_generated: boolean;
    }

    export interface PassAuth {
        password?: string;
        username?: string;
    }

    export interface TokenAuth {
        uid?: string;
        token?: string;
    }

    export interface Account {
        owner?: boolean;
        username?: string;
        admin?: boolean;
    }

    export interface Notification {
        ID: string;
        title: string;
        notify: string;
    }

    export interface Short {
        link: string;
        owner: string;
        ID: string;
    }

    export interface File {
        file: Buffer;
        name: string;
    }

    export type Uploadable = File | string;

    class Client {
        config: any;
        account: Account;
        private passAuth: PassAuth;
        private tokenAuth: TokenAuth;
        constructor(config: Config);

        private _initConfig(): void;

        private _refreshAcc(): void;

        init(): Promise<Config>;

        updateConfig(config: Config | string, key?: string): Promise<Config>;

        addAdmin(uID: string): Promise<string>;

        clearNotifications(): Promise<string>;

        createShort(url: string): Promise<string>;

        deleteAccount(uID?: string): Promise<string>;

        deleteAdminNotification(ID: string): Promise<string>;

        deleteImage(ID: string): Promise<string>;

        deleteNotification(ID: string): Promise<string>;

        deleteShort(ID: string): Promise<string>;

        denyAccount(uID: string, verifyToken: string): Promise<string>;

        fetchAccount(): Promise<AccountResponse>;

        fetchAdminNotification(ID: string): Promise<Notification>;

        fetchImages(): Promise<string[] | []>;

        fetchNotification(ID: string): Promise<Notification>;

        fetchNotifications(admin?: boolean): Promise<Notification[] | []>;

        fetchShorts(): Promise<Short | []>;

        removeAdmin(uID: string): Promise<string>;

        signup(username: string, password: string): Promise<string>;

        updateAccount(key: string, newKey: string): Promise<string>;

        updateToken(force?: boolean): Promise<string>;

        uploadImage(file: Uploadable): Promise<string>

        verifyAccount(uID: string, verifyToken: string): Promise<string>;
    }
    export default Client;

}
