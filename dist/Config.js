/**
 * @class ClientConfig
 *
 * @author VoidNulll
 */
class ClientConfig {
    constructor(config) {
        this.url = config.url;
        this.username = config.username;
        this.password = config.password;
        this.token = config.token;
        this.uID = config.uID;
    }
}
export default ClientConfig;
